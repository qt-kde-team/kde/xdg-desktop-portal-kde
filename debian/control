Source: xdg-desktop-portal-kde
Section: kde
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Aurélien COUDERC <coucouf@debian.org>,
           Patrick Franz <deltaone@debian.org>,
           Scarlett Moore <sgmoore@kde.org>,
Build-Depends: debhelper-compat (= 13),
               dh-sequence-kf6,
               dh-sequence-pkgkde-symbolshelper,
               cmake (>= 3.16~),
               extra-cmake-modules (>= 6.10.0~),
               gettext,
               kwayland-dev (>= 4:6.3.2~),
               libcups2-dev,
               libkf6config-dev (>= 6.10.0~),
               libkf6coreaddons-dev (>= 6.10.0~),
               libkf6crash-dev (>= 6.10.0~),
               libkf6globalaccel-dev (>= 6.10.0~),
               libkf6guiaddons-dev (>= 6.10.0~),
               libkf6i18n-dev (>= 6.10.0~),
               libkf6iconthemes-dev (>= 6.10.0~),
               libkf6kio-dev (>= 6.10.0~),
               libkf6notifications-dev (>= 6.10.0~),
               libkf6service-dev (>= 6.10.0~),
               libkf6statusnotifieritem-dev (>= 6.10.0~),
               libkf6widgetsaddons-dev (>= 6.10.0~),
               libkf6windowsystem-dev (>= 6.10.0~),
               libkirigami-dev (>= 6.10.0~),
               libwayland-dev (>= 1.15~),
               plasma-wayland-protocols (>= 1.7.0~),
               qt6-base-dev (>= 6.7.0~),
               qt6-declarative-dev (>= 6.7.0~),
               qt6-wayland-dev (>= 6.7.0~),
               wayland-protocols (>= 1.25~),
Standards-Version: 4.7.0
Homepage: https://invent.kde.org/plasma/xdg-desktop-portal-kde
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/xdg-desktop-portal-kde
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/xdg-desktop-portal-kde.git
Rules-Requires-Root: no

Package: xdg-desktop-portal-kde
Architecture: any
Multi-Arch: foreign
Depends: kio-fuse,
         qml6-module-org-kde-iconthemes,
         xdg-desktop-portal,
         ${misc:Depends},
         ${shlibs:Depends},
Provides: xdg-desktop-portal-backend,
Description: backend implementation for xdg-desktop-portal using Qt
 xdg-desktop-portal-kde provides a Qt implementation for the
 desktop-agnostic xdg-desktop-portal service. This allows sandboxed
 applications to request services from outside the sandbox using KDE
 GUIs (app chooser, file chooser, print dialog) or using KDE services
 (session manager, screenshot provider).
